class Site < ActiveRecord::Base

def version
  # document = Nokogiri::XML(open(self.url))
  # releases = document.xpath("//release", "xmlns" => "http://purl.org/dc/elements/1.1/")
  # return releases[0].xpath('version').inner_text
  text = open(self.url) { |io| io.read }
  version = text.split(",").first.split(" ")
  return version[1]
end

end
