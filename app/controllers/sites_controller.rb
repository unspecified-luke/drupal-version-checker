class SitesController < ApplicationController

def index
  @sites = Site.all
end

def new
  @site = Site.new
end

def create
  @site = Site.create(site_params)
  redirect_to @site
end

def show
  @site = Site.find(params[:id])
end

def edit
  @site = Site.find(params[:id])
end

def update
  @site = Site.find(params[:id])
  @site.update(site_params)
  redirect_to @site
end

def destroy
  s = Site.find(params[:id])
  s.destroy
  redirect_to sites_path
end

private

  def site_params (params = self.params)
    params.require(:site).permit(:name, :url)
  end

end
