class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :current_version

  require 'open-uri'

  def index
    @current = current_version
  end

  def current_version
    document = Nokogiri::XML(open("http://updates.drupal.org/release-history/drupal/7.x"))
    # Load the data from the XML file into a variable

    releases = document.xpath("//release", "xmlns" => "http://purl.org/dc/elements/1.1/")
    # Creates an array of all the releases specified in the document

    @current_version = releases[0].xpath('version').inner_text
    # Set's the current_version to the version number of the first release in the file. Asssumed to be the most recent. 

    @security_release
    # Declare an instance variable for security_release

    releases.each do |r|
      # Iterate through the releases

      if !@security_release.blank?
        break
      end
      # If the security release has been specified, end the iteration of releases as it's no longer needed.

      version = r.xpath('version').inner_text
      # Store the version number of the current release

      r.xpath("//term", "xmlns" => "http://purl.org/dc/elements/1.1/").each do |t|
        # Iterate through this releases terms

        if @security_release.blank?
          # If the security release hasn't been specified yet, continue.

          if t.children[1].inner_text.downcase == "security update"
            # If the 'value' of the current term is "Security Update"

            @security_release = version
            # Set the value of "security_release" with the version numbero of the current release

          end
        end
      end
    end 

  end

end
